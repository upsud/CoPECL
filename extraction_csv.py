#!/usr/bin/env python3

import sys
from rdflib import Graph, Literal, BNode, Namespace, RDF, URIRef, plugin
from rdflib.namespace import DCTERMS, XSD, NamespaceManager
from rdflib.store import Store, NO_STORE, VALID_STORE
COPECL = Namespace("https://www.u-psud.fr/CoPECL#")
URI_PREFIX = "https://www.u-psud.fr/CoPECL/"
PAYS = {'1':'Communauté européenne', '2':'Allemagne', '3':'Grande-Bretagne', '7':'Espagne', '9':'France', '13':'Irlande'}
TYPES_PRINCIPES = {'1':'Chapitre', '2':'Section', '3':'Article'}
MOTSCLES = {'1':'Conclusion du contrat', '2':'Conditions', '4':'Contrat de remplacement', '5':'Contrat(s)',
                '6':'Contrats à exécution échelonnée, fractionnée', '7':'Déchéance d\'un droit ; exception',
                '8':'Détermination du prix', '9':'Dette de somme d\'argent', '10':'Devoir de collaboration, coopération',
                '31':'Notification', '100':'Determination of price', '122':'Mitigation of damages', '201':'Alternativverbindlichkeit',
                '264':'Misrepresentation', '265':'Acceptance', '266':'Reduction of loss', '267':'Angebot', '268':'Spania',
                '269':'Pago', '270':'Acceptation'}
# Adresse endpoint
graph_uri = "http://rech.u-psud.fr/CoPECL/rdfstore"
db_location = "/var/tmp/rdfstore"


class CoPECLElement:
    def __init__(self, ident=None):
        self.uri = None
        for attr in self.LISTE_ATTRIBUTS:
            setattr(self, attr, None)
        if ident is not None:
            self.ident = ident
    def set(self, liste_valeurs):
        for i in range(len(self.LISTE_ATTRIBUTS)):
            attr = self.LISTE_ATTRIBUTS[i]
            setattr(self, attr, liste_valeurs[i])

class Decision(CoPECLElement):
    LISTE_ATTRIBUTS = ['ident','id_pays','date_decision','classification','resume_fr','resume_gb','commentaire_fr','commentaire_gb','ref_biblio_fr','ref_biblio_gb','doctrine_fr','doctrine_gb','parties','origine_decision']
    LISTE_RDFTERMS = [None, DCTERMS.Jurisdiction, DCTERMS.date, COPECL.classification, DCTERMS.abstract, DCTERMS.abstract, COPECL.comment, COPECL.comment, DCTERMS.bibliographicCitation, DCTERMS.bibliographicCitation, DCTERMS.references, DCTERMS.references, COPECL.parties, DCTERMS.creator]
    def make_uri(self):
        if self.ident:
            self.uri = URI_PREFIX + "CR%s" % self.ident

class Principe(CoPECLElement):
    LISTE_ATTRIBUTS = ['ident', 'num_chapitre', 'num_section', 'num_article', 'libelle_fr', 'libelle_gb', 'id_type_principe']
    LISTE_RDFTERMS = [None, DCTERMS.isPartOf, DCTERMS.isPartOf, None, DCTERMS.title, DCTERMS.title, DCTERMS.type]
    def make_uri(self):
        if self.ident:
            self.uri = URI_PREFIX + "P%s" % self.ident

# Création du graphe
g = Graph()
z = NamespaceManager(g)
z.bind('dcterms', DCTERMS)
z.bind('copecl', COPECL)

# Dictionnaire des relations avec les mots-clés
text = open("icd/rel_motsclefs_principes.txt").read()
lines = text.split('µ@')
relations_principes_motsclefs = {}

for l in lines:
    l = l.replace('Â', '').strip()
    if not l:
        continue
    id_principe, id_mot_clef, id_langue = l.split('µ')
    if not id_principe in relations_principes_motsclefs:
        relations_principes_motsclefs[id_principe] = []
    motcle = MOTSCLES[id_mot_clef]
    if id_mot_clef in [100,122,264,265,266]:
        langue = 'en'
    elif id_mot_clef in [201,267]:
        langue = 'de'
    elif id_mot_clef in [268,269]:
        langue = 'es'
    else:
        langue = 'fr'
    relations_principes_motsclefs[id_principe].append((motcle, langue))


# Principes
text = open("icd/principes.txt").read()
lines = text.split('µ@')
# D'abord repérer les chapitres et les sections
ids_chapitres = {}
ids_sections = {}
for l in lines:
    l = l.replace('Â', '').strip()
    if not l:
        continue
    ll = l.split('µ')
    try:
        ident, num_chapitre, num_section, num_article = int(ll[0]), int(ll[1]), int(ll[2]), int(ll[3])
    except:
        print(ll)
    if not num_section and not num_article:
        ids_chapitres[num_chapitre] = ident
    elif not num_article:
        ids_sections[(num_chapitre, num_section)] = ident
# Puis l'analyse entière
for l in lines:
    l = l.replace('Â', '').strip()
    if not l:
        continue
    p = Principe()
    p.set([ x.strip() for x in l.split('µ') ])
    p.make_uri()
    u = URIRef(p.uri)
    g.add( (u, RDF.type, COPECL.PECL) )
    if p.ident in relations_principes_motsclefs.keys():
        for (mot, langue) in relations_principes_motsclefs[p.ident]:
            g.add( (u, DCTERMS.subject, Literal(mot, lang = "%s" % langue)) )
    for i in range(len(p.LISTE_ATTRIBUTS)):
        a = p.LISTE_ATTRIBUTS[i]
        if getattr(p, a) and p.LISTE_RDFTERMS[i]:
            valeur = getattr(p,a)
            rdfterm = p.LISTE_RDFTERMS[i]
            datatype = None
            langue = None
            if a == 'id_type_principe':
                valeur = TYPES_PRINCIPES[p.id_type_principe]
            elif a == 'num_chapitre':
                """Le numéro de chapitre n'est jamais zéro dans la base"""
                c = Principe(getattr(p, a))
                c.make_uri()
                valeur = c.uri
            elif a == 'num_section':
                if not int(getattr(p, a)):
                    continue
                else:
                    """Seulement si le numéro de section n'est pas zéro"""
                    s = Principe(getattr(p, a))
                    s.make_uri()
                    valeur = s.uri
            elif a.endswith('fr'):
                langue = 'fr'
            elif a.endswith('gb'):
                langue = 'en'
            if langue:
                g.add( (u, rdfterm, Literal(valeur, lang = "%s" % langue)) )
            elif datatype:
                g.add( (u, rdfterm, Literal(valeur, datatype=datatype)) )
            else:
                g.add( (u, rdfterm, Literal(valeur)) )

# Dictionnaire des relations décisions -> principes
text = open("icd/rel_decisions_principes.txt").read()
lines = text.split('µ@')
relations_decisions_principes = {}

for l in lines:
    l = l.replace('Â', '').strip()
    if not l:
        continue
    id_decision, id_principe = l.split('µ')
    if not id_decision in relations_decisions_principes:
        relations_decisions_principes[id_decision] = []
    relations_decisions_principes[id_decision].append(id_principe)

# Décisions
text = open("icd/decisions.txt").read()
lines = text.split('µ@')
for l in lines:
    l = l.replace('Â', '').strip()
    if not l:
        continue
    d = Decision()
    d.set([ x.strip() for x in l.split('µ') ])
    d.make_uri()
    u = URIRef(d.uri)
    g.add( (u, RDF.type, COPECL.CourtRuling) )
    if d.ident in relations_decisions_principes.keys():
        for id_principe in relations_decisions_principes[d.ident]:
            p = Principe(id_principe)
            d.make_uri()
            g.add( (u, COPECL.relatesToPrinciple, Literal(d.uri)) )
    localisation_decision = None
    for i in range(len(d.LISTE_ATTRIBUTS)):
        a = d.LISTE_ATTRIBUTS[i]
        if getattr(d, a) and d.LISTE_RDFTERMS[i]:
            valeur = getattr(d,a)
            rdfterm = d.LISTE_RDFTERMS[i]
            datatype = None
            langue = None
            if a == 'id_pays':
                id_pays = d.id_pays
                if id_pays == '0':
                    id_pays = '3'
                valeur = PAYS[id_pays]
                langue = 'fr'
                localisation_decision = PAYS[id_pays]
            elif a == 'date_decision':
                datatype = XSD.date
            elif a == 'parties':
                if localisation_decision == 'Allemagne':
                    langue = 'de'
                elif localisation_decision == 'Espagne':
                    langue = 'es'
                elif localisation_decision == 'France':
                    langue = 'fr'
                else:
                    langue = 'en'
            elif a.endswith('fr'):
                langue = 'fr'
            elif a.endswith('gb'):
                langue = 'en'
            if langue:
                g.add( (u, rdfterm, Literal(valeur, lang = "%s" % langue)) )
            elif datatype:
                g.add( (u, rdfterm, Literal(valeur, datatype=datatype)) )
            else:
                g.add( (u, rdfterm, Literal(valeur)) )


with open('CoPECL.n3', 'wb') as out_file: # Binary mode
    xml_bytes = g.serialize(format='n3', encoding='utf-8')
    out_file.write(xml_bytes)
with open('CoPECL.ttl', 'wb') as out_file: # Binary mode
    xml_bytes = g.serialize(format='turtle', encoding='utf-8')
    out_file.write(xml_bytes)
with open('CoPECL.xml', 'wb') as out_file: # Binary mode
    xml_bytes = g.serialize(format='xml', encoding='utf-8')
    out_file.write(xml_bytes)


# Get the Sleepycat plugin
store = plugin.get('Sleepycat', Store)('rdfstore')

# Open previously created store, or create it if it doesn't exist yet
graph = Graph(store="Sleepycat", identifier = URIRef(graph_uri))

rt = None
try:
    rt = graph.open(db_location, create=False)
except:
    pass
if not rt or rt == NO_STORE:
    # There is no underlying Sleepycat infrastructure, create it
    graph.open(db_location, create=True)
else:
    assert rt == VALID_STORE, "The underlying store is corrupt"
print ("Triples in graph before add: ", len(graph))
graph = graph + g
graph.commit()
print ("Triples in graph after add: ", len(graph))

# Clean up the mkdtemp spoor to remove the Sleepycat database files...
#import os
#for f in os.listdir(db_location):
#    os.unlink(db_location + '/' + f)

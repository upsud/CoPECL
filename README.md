CoPECL Data
===========
This is an ontology for a dataset related to european project CoPECL (CIT3-CT-2005-513351)

It was generated with Python rdflib

To visualize it graphically, if you have rdflib and dot on your computer:

$ /path_to/rdfs2dot.py CoPECL.rdf | dot -Tpng | display
